using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonajeControler : MonoBehaviour {


    private Rigidbody2D rb;
    private Animator animator;
    private SpriteRenderer sr;

    // Start is called before the first frame update
    void Start()
    {

        sr = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }


    // Update is called once per frame
    void Update()
    {


        if (Input.GetKey(KeyCode.RightArrow))
        { //Derecha
            sr.flipX = false;
            animator.SetInteger("Estado", 1);
            rb.velocity = new Vector2(5, rb.velocity.y);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
         { 
            sr.flipX = true;
            animator.SetInteger("Estado", 1);
            rb.velocity = new Vector2(-5, rb.velocity.y);
        }
        if (Input.GetKey(KeyCode.Z))
        {
            
            animator.SetInteger("Estado", 3);
           
        }
        if ((Input.GetKey(KeyCode.LeftArrow)) && (Input.GetKey(KeyCode.X)))
        {
            sr.flipX = true;
            animator.SetInteger("Estado", 4);
            rb.velocity = new Vector2(-5, rb.velocity.y);
        }
        if ((Input.GetKey(KeyCode.RightArrow)) && (Input.GetKey(KeyCode.X)))
        {
            sr.flipX = false;
            animator.SetInteger("Estado", 4);
            rb.velocity = new Vector2(5, rb.velocity.y);
        }

        if (Input.GetKey(KeyCode.Space))
        {

            animator.SetInteger("Estado", 2);

        }
    }

}